// Copyright (c) 2011-2016 The Cryptonote developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#pragma once

namespace CryptoNote
{
  const static boost::uuids::uuid CRYPTONOTE_NETWORK = { { 0x33, 0x27, 0x2f, 0xb3, 0xa8, 0x2b, 0xfb, 0xcc, 0x69, 0xe6, 0x78, 0xcb, 0x16, 0xff, 0x34, 0x28 } };

}
